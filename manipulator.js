/**
 *  To create DOM manipulation library
 */

// Getter, Setter for current object
var getCurr, setCurr;
(
   function () {
       var obj = document.body;
       getCurr = function() {
           return obj;
       };
       setCurr = function (id) {
            // check whether node exists in body or not
           var element = getElement(id);
           if (element && document.body.contains(element)) {
               obj = element;
           } else {
               console.log('!!!!! Error, cannot set element with id ' + id + ' as current object as it does not exists');
           }
       }
   }
)();


function getElement(id) {
    return document.getElementById(id);
}

function manipulator() {
    return {
        createElement : function(node, id) {
            // @TODO check whether valid HTML element is being created or not
            var element = document.createElement(node);
            // Set the id of element
            if (id) {
                element.id = id;
            }
            return element;
        },
        getElementById : function (id) {
            return getElement(id);
        },
        getHTMLContent : function (id) {
            var element = getElement(id);
            if (element) {
                return element.innerHTML;
            } else {
                console.log('Element with id: ' + id + ' not found');
            }
        },
        setHTMLContent : function (id, content) {
            var element = getElement(id);
            if (element && content) {
                element.innerHTML = content;
            } else {
                console.log('Element with id: ' + id + ' not found');
            }
        },
        addElement : function (elementId, child) {
            var element =  getElement(elementId);
            if (element) {
                return element.appendChild(child);
            } else {
                console.log('Element with id: ' + elementId + ' not found');
            }
        },

    }
}
